package com.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

	@RequestMapping(value="/")
	public ModelAndView main(ModelAndView model) {
		model.setViewName("main");
		return model;
	}
	
	@RequestMapping(value = "//{pageNm}.html") 
	public ModelAndView pageCall(@PathVariable String pageNm, ModelAndView model, HttpServletRequest req ) throws Exception{ 
	model.setViewName(pageNm);
	return model;
}
	
	
	
}
